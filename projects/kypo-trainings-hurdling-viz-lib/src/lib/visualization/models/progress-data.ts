export interface ProgressData {
  firstEvent;
  estimatedDuration: number;
  lastEvent;
}
