export class Hint {
  id: number;
  title: string;
  content: string;
}
