export class WrongAnswerData {
  value: string;
  timesUsed: number;
  lastUsed: string;
}
