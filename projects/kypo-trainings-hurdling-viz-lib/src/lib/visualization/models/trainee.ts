export class Trainee {
  userRefId: number;
  trainingRunId: number;
  name: string;
  picture: string;
  teamIndex: number;
}
