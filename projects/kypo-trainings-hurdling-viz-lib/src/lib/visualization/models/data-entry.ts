export class DataEntry {
  traineeName: string;
  event: string;
  level: number;
  time: number;
  timestamp: number;
}
